/*
 *  Copyright 2019, The Android Open Source Project
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.example.android.marsrealestate.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.R
import com.example.android.marsrealestate.network.MarsProperty

/**
 * The [ViewModel] that is associated with the [DetailFragment].
 */
class DetailViewModel(
  @Suppress("UNUSED_PARAMETER") marsProperty: MarsProperty,
  app: Application
) : AndroidViewModel(app) {

  private val _selectedProp = MutableLiveData<MarsProperty>()

  val selectedProp: LiveData<MarsProperty>
    get() = _selectedProp

  init {
    _selectedProp.value = marsProperty
  }

  /** This transformation tests whether the selected property is a rental,
   * using the same test from the first task. If the property is a rental,
   * the transformation chooses the appropriate string from the resources
   * with a Kotlin when {} switch. Both of these strings need a number at
   * the end, so we concatenate the property.price afterwards.
   * */

  val displayPropPrice = Transformations.map(selectedProp) {
    app.applicationContext.getString(
      when (it.isRental) {
        true -> R.string.display_price_monthly_rental
        false -> R.string.display_price
      }, it.price
    )
  }

  val displayPropType = Transformations.map(selectedProp) {
    app.applicationContext.getString(
      R.string.display_type,
      app.applicationContext.getString(
        when (it.isRental) {
          true -> R.string.type_rent
          false -> R.string.type_sale
        }
      )
    )
  }

}